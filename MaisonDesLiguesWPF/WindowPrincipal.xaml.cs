﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaisonDesLiguesWPF.Model;
using MaisonDesLiguesWPF;
using System.Data;
using System.Collections.ObjectModel;

namespace MaisonDesLiguesWPF
{
    /// <summary>
    /// Logique d'interaction pour WindowPrincipal.xaml
    /// </summary>
    public partial class WindowPrincipal : MetroWindow
    {
        private OracleDataBase UneConnexion;
        private String TitreApplication;
        private String IdStatutSelectionne = "";

        /// <summary>
        /// constructeur de la fenêtre
        /// </summary>
        public WindowPrincipal()
        {
            InitializeComponent();
        }

        /// <summary>
        /// création et ouverture d'une connexion vers la base de données sur le chargement de la fenêtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            UneConnexion = ((WindowLogin)Owner).oracleContext;
            TitreApplication = ((WindowLogin)Owner).TitreApplication;
            this.Title = TitreApplication;
            grpBenevole.Visibility = Visibility.Hidden;
            grpIntervenant.Visibility = Visibility.Hidden;
        }

        /// <summary>     
        /// procédure permettant d'afficher l'interface de saisie du complément d'inscription d'un intervenant.
        /// </summary>
        private void GererInscriptionIntervenant()
        {
            grpBenevole.Visibility = Visibility.Hidden;
            grpIntervenant.Visibility = Visibility.Visible;
            panFonctionIntervenant.Visibility = Visibility.Visible;
            if (panFonctionIntervenant.Children.Count == 0)
            {
                Utilitaire.CreerDesControles(this, UneConnexion, UneConnexion.VSTATUT01.ToList(), "Rad_", panFonctionIntervenant, "RadioButton", this.rdbStatutIntervenant_StateChanged);
            }
            Utilitaire.RemplirComboBox(cmbAtelierIntervenant, UneConnexion.VATELIER01);

            cmbAtelierIntervenant.Text = "Choisir";

        }
        /// <summary>     
        /// procédure permettant d'afficher l'interface de saisie des disponibilités des bénévoles.
        /// </summary>
        private void GererInscriptionBenevole()
        {
            grpBenevole.Visibility = Visibility.Visible;
            grpBenevole.Margin = new Thickness(10, 380, 0, 0);
            grpIntervenant.Visibility = Visibility.Hidden;

            if (panelDispoBenevole.Children.Count == 0)
            {
                Utilitaire.CreerDesControles(this, UneConnexion, UneConnexion.VDATEBENEVOLAT01.ToList(), "ChkDateB_", this.panelDispoBenevole, "CheckBox", this.rdbStatutIntervenant_StateChanged);
                // on va tester si le controle à placer est de type CheckBox afin de lui placer un événement checked_changed
                // Ceci afin de désactiver les boutons si aucune case à cocher du container n'est cochée
                foreach (UIElement UnControle in panelDispoBenevole.Children)
                {
                    if (UnControle.GetType().Name == "CheckBox")
                    {
                        CheckBox UneCheckBox = (CheckBox)UnControle;
                        UneCheckBox.Checked += new RoutedEventHandler(this.ChkDateBenevole_CheckedChanged);
                        UneCheckBox.Unchecked += new RoutedEventHandler(this.ChkDateBenevole_CheckedChanged);
                    }
                }
            }


        }

        /// <summary>
        /// Cette méthode teste les données saisies afin d'activer ou désactiver le bouton d'enregistrement d'un bénévole
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkDateBenevole_CheckedChanged(object sender, EventArgs e)
        {
            btnEnregistreBenevole.IsEnabled = (txtLicenceBenevole.Text == "" || txtLicenceBenevole.IsMaskCompleted) && txtDateNaissance.IsMaskCompleted && Utilitaire.CompteChecked(panelDispoBenevole) > 0;
        }

        /// <summary>
        /// permet d'appeler la méthode VerifBtnEnregistreIntervenant qui déterminera le statu du bouton BtnEnregistrerIntervenant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdbStatutIntervenant_StateChanged(object sender, EventArgs e)
        {
            // stocke dans un membre de niveau form l'identifiant du statut sélectionné (voir règle de nommage des noms des controles : prefixe_Id)
            this.IdStatutSelectionne = ((RadioButton)sender).Name.Split('_')[1];
            btnEnregistrerIntervenant.IsEnabled = VerifBtnEnregistreIntervenant();
        }

        /// <summary>
        /// Méthode privée testant le contrôle combo et la variable IdStatutSelectionne qui contient une valeur
        /// Cette méthode permetra ensuite de définir l'état du bouton BtnEnregistrerIntervenant
        /// </summary>
        /// <returns></returns>
        private Boolean VerifBtnEnregistreIntervenant()
        {
            if (cmbAtelierIntervenant.ItemsSource != null)
            {
                object itemSelectionner = cmbAtelierIntervenant.SelectedItem;
                if (itemSelectionner != null)
                {
                    return ((VATELIER01)itemSelectionner).LIBELLE != "Choisir" && this.IdStatutSelectionne.Length > 0;
                }
            }
            return false;
        }

        private void RadTypeParticipant_Changed(object sender, RoutedEventArgs e)
        {
            switch (((RadioButton)sender).Name)
            {
                case "RadBenevole":
                    this.GererInscriptionBenevole();
                    break;
                case "RadLicencie":
                    //this.GererInscriptionLicencie();
                    break;
                case "RadIntervenant":
                    this.GererInscriptionIntervenant();
                    break;
                default:
                    throw new Exception("Erreur interne à l'application");
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Méthode qui permet d'afficher ou masquer le controle panel permettant la saisie des nuités d'un intervenant.
        /// S'il faut rendre visible le panel, on teste si les nuités possibles ont été chargés dans ce panel. Si non, on les charges 
        /// On charge ici autant de contrôles ResaNuit qu'il y a de nuits possibles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RdbNuiteIntervenant_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (((RadioButton)sender).Name == "rdbNuiteIntervenantOui")
            {
                panNuiteIntervenant.Visibility = Visibility.Visible;
                if (panNuiteIntervenant.Children.Count == 0) // on charge les nuites possibles et on les affiche
                {

                    Utilitaire.creationNuitee(UneConnexion, panNuiteIntervenant);

                }

            }
            else
            {
                panNuiteIntervenant.Visibility = Visibility.Hidden;
            }
            btnEnregistrerIntervenant.IsEnabled = VerifBtnEnregistreIntervenant();
        }

        /// <summary>
        /// Cette procédure va appeler la procédure .... qui aura pour but d'enregistrer les éléments 
        /// de l'inscription d'un intervenant, avec éventuellment les nuités à prendre en compte        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnregistrerIntervenant_Click(object sender, RoutedEventArgs e)
        {
            Collection<Panel> lesGrids = new Collection<Panel>();
            lesGrids.Add(gridIdentite);
            lesGrids.Add(gridIntervenant);
            lesGrids.Add(panFonctionIntervenant);
            lesGrids.Add(gridNuiteeIntervenant);
            try
            {
                if (this.controleChampsIdentite())
                {
                    if ((bool)rdbNuiteIntervenantOui.IsChecked)
                    {
                        // inscription avec les nuitées
                        Collection<Int16> NuitsSelectionnes = new Collection<Int16>();
                        Collection<String> HotelsSelectionnes = new Collection<String>();
                        Collection<String> CategoriesSelectionnees = new Collection<string>();
                        foreach (UIElement UnControle in panNuiteIntervenant.Children)
                        {
                            if (UnControle.GetType().Name == "ResaNuite" && ((ResaNuite)UnControle).getNuitSelectionnee())
                            {
                                // la nuité a été cochée, il faut donc envoyer l'hotel et la type de chambre à la procédure de la base qui va enregistrer le contenu hébergement 
                                //ContenuUnHebergement UnContenuUnHebergement= new ContenuUnHebergement();
                                CategoriesSelectionnees.Add(((ResaNuite)UnControle).getTypeChambreSelectionnee());
                                HotelsSelectionnes.Add(((ResaNuite)UnControle).getHotelSelectionne());
                                NuitsSelectionnes.Add(((ResaNuite)UnControle).getIdNuite());
                            }

                        }
                        if (NuitsSelectionnes.Count == 0)
                        {
                            MessageBox.Show("Si vous avez sélectionné que l'intervenant avait des nuités\n il faut qu'au moins une nuit soit sélectionnée");
                        }
                        else
                        {
                            UneConnexion.InscrireIntervenant(txtNom.Text, txtPrenom.Text, txtAdr1.Text, txtAdr2.Text != "" ? txtAdr2.Text : null, txtCp.Text, txtVille.Text, txtTel.IsMaskCompleted ? txtTel.Text : null, txtMail.Text != "" ? txtMail.Text : null, System.Convert.ToInt16(cmbAtelierIntervenant.SelectedValue), this.IdStatutSelectionne, CategoriesSelectionnees, HotelsSelectionnes, NuitsSelectionnes);
                            Utilitaire.razChamps(lesGrids);
                            MessageBox.Show("Inscription intervenant effectuée");
                        }
                    }
                    else
                    { // inscription sans les nuitées
                        UneConnexion.InscrireIntervenant(txtNom.Text, txtPrenom.Text, txtAdr1.Text, txtAdr2.Text != "" ? txtAdr2.Text : null, txtCp.Text, txtVille.Text, txtTel.IsMaskCompleted ? txtTel.Text : null, txtMail.Text != "" ? txtMail.Text : null, System.Convert.ToInt16(cmbAtelierIntervenant.SelectedValue), this.IdStatutSelectionne);
                        Utilitaire.razChamps(lesGrids);
                        MessageBox.Show("Inscription intervenant effectuée");

                    }
                }
                else
                {
                    MessageBox.Show("Un/Plusieurs champs n'a/ont pas été renseigné");
                }


            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }
        /// <summary>
        /// Permet d'intercepter le click sur le bouton d'enregistrement d'un bénévole.
        /// Cetteméthode va appeler la méthode InscrireBenevole de la Bdd, après avoir mis en forme certains paramètres à envoyer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnregistreBenevole_Click(object sender, RoutedEventArgs e)
        {
            Collection<Panel> lesGrids = new Collection<Panel>();
            lesGrids.Add(gridIdentite);
            lesGrids.Add(gridDispoBenevole);
            lesGrids.Add(panelDispoBenevole);
            try
            {
                if (this.controleChampsIdentite() && txtLicenceBenevole.IsMaskCompleted && txtDateNaissance.IsMaskCompleted )
                {
                    Collection<Int16> IdDatesSelectionnees = new Collection<Int16>();
                    Int64? NumeroLicence;
                    if (txtLicenceBenevole.IsMaskCompleted)
                    {
                        NumeroLicence = System.Convert.ToInt64(txtLicenceBenevole.Text);
                    }
                    else
                    {
                        NumeroLicence = null;
                    }


                    foreach (Control UnControle in panelDispoBenevole.Children)
                    {
                        if (UnControle.GetType().Name == "CheckBox" && (bool)((CheckBox)UnControle).IsChecked)
                        {
                            /* Un name de controle est toujours formé come ceci : xxx_Id où id représente l'id dans la table
                             * Donc on splite la chaine et on récupére le deuxième élément qui correspond à l'id de l'élément sélectionné.
                             * on rajoute cet id dans la collection des id des dates sélectionnées

                            */
                            IdDatesSelectionnees.Add(System.Convert.ToInt16((UnControle.Name.Split('_'))[1]));
                        }
                    }

                    UneConnexion.InscrireBenevole(txtNom.Text, txtPrenom.Text, txtAdr1.Text, txtAdr2.Text != "" ? txtAdr2.Text : null, txtCp.Text, txtVille.Text, txtTel.IsMaskCompleted ? txtTel.Text : null, txtMail.Text != "" ? txtMail.Text : null, System.Convert.ToDateTime(txtDateNaissance.Text), NumeroLicence, IdDatesSelectionnees);
                    Utilitaire.razChamps(lesGrids);
                    MessageBox.Show("Inscription du bénévole effectuée");
                }
                else
                {
                    MessageBox.Show("Un/Plusieurs champs n'a/ont pas été renseigné");
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void btnQuitter_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        /// <summary>
        /// Méthode permettant de définir le statut activé/désactivé du bouton BtnEnregistrerIntervenant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAtelierIntervenant_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnEnregistrerIntervenant.IsEnabled = VerifBtnEnregistreIntervenant();
        }
        /// <summary>
        /// Contrôle pour vérifier que les champs du groupbox identité sont bien remplis
        /// </summary>
        /// <returns>Vrai si ils sont tous remplis</returns>
        private bool controleChampsIdentite()
        {
            return txtAdr1.Text != "" && txtCp.IsMaskCompleted && txtNom.Text != "" && txtPrenom.Text != "" && txtVille.Text != "";
        }
    }
}
