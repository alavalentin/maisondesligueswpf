﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Reflection;
using MaisonDesLiguesWPF.Model;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using MahApps.Metro.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Collections;
using System.Data.Entity;
using Xceed.Wpf.Toolkit;

namespace MaisonDesLiguesWPF
{
    /// <summary>
    /// Classe contenant les méthodes utiles à l'affichage de données
    /// </summary>
    public abstract class Utilitaire
    {

        /// <summary>
        /// méthode permettant de renvoyer un message d'erreur provenant de la bd
        /// après l'avoir formatté. On ne renvoie que le message, sans code erreur
        /// </summary>
        /// <param name="unMessage">message à formater</param>
        /// <returns>message formaté à afficher dans l'application</returns>
        public static String GetMessageOracle(String unMessage)
        {
            String[] message = Regex.Split(unMessage, "ORA-");
            return (Regex.Split(message[1], ":"))[1];
        }

        /// <summary>
        /// Cette méthode permet de renseigner les propriétés des contrôles à créer. C'est une partie commune aux 
        /// 3 types de participants : intervenant, licencié, bénévole
        /// </summary>
        /// <param name="UneForme">le formulaire concerné</param>  
        /// <param name="UnContainer">le panel ou le groupbox dans lequel on va placer les controles</param> 
        /// <param name="UnControleAPlacer"> le controle en cours de création</param>
        /// <param name="UnPrefixe">les noms des controles sont standard : NomControle_XX
        ///                                         ou XX estl'id de l'enregistrement récupéré dans la vue qui
        ///                                         sert de siurce de données</param> 
        /// <param name="UneLigne">un enregistrement de la vue, celle pour laquelle on crée le contrôle</param> 
        /// <param name="i"> Un compteur qui sert à positionner en hauteur le controle</param>   
        /// <param name="callback"> Le pointeur de fonction. En fait le pointeur sur la fonction qui réagira à l'événement déclencheur </param>
        private static void AffecterControle(MetroWindow UneForme, Panel UnContainer, ButtonBase UnControleAPlacer, String UnPrefixe, Object UneLigne, Int16 i, Action<object, EventArgs> callback)
        {
            UnControleAPlacer.Name = UnPrefixe + ((IVue)UneLigne).getId();
            UnControleAPlacer.Width = 320;
            UnControleAPlacer.Height = 30;
            UnControleAPlacer.Content = ((IVue)UneLigne).getLibelle();
            UnControleAPlacer.Visibility = Visibility.Visible;
            System.Type UnType = UneForme.GetType();
            UnContainer.Children.Add(UnControleAPlacer);

        }

        /// <summary>
        /// Cette méthode crée des controles de type chckbox ou radio button dans un controle de type panel.
        /// Elle va chercher les données dans la base de données et crée autant de controles (les uns au dessous des autres
        /// qu'il y a de lignes renvoyées par la base de données.
        /// </summary>
        /// <param name="uneWindows">Le formulaire concerné</param> 
        /// <param name="UneConnexion">L'objet connexion à utiliser pour la connexion à la BD</param> 
        /// <param name="listeEntitee">Le nom de la source de données qui va fournir les données. Il s'agit en fait d'une vue de type
        /// VXXXXOn ou XXXX représente le nom de la tabl à partir de laquelle la vue est créée. n représente un numéro de séquence</param>  
        /// <param name="pPrefixe">les noms des controles sont standard : NomControle_XX
        ///                                         ou XX estl'id de l'enregistrement récupéré dans la vue qui
        ///                                         sert de source de données</param>
        /// <param name="UnPanel">panel ou groupbox dans lequel on va créer les controles</param>
        /// <param name="unTypeControle">type de controle à créer : checkbox ou radiobutton</param>
        /// <param name="callback"> Le pointeur de fonction. En fait le pointeur sur la fonction qui réagira à l'événement déclencheur </param>
        public static void CreerDesControles<T>(MetroWindow uneWindows, OracleDataBase UneConnexion, List<T> listeEntitee, String pPrefixe, Panel UnPanel, String unTypeControle, Action<object, EventArgs> callback) where T : class
        {
            //DataTable UneTable = uneConnexion.ObtenirDonnesOracle(pUneTable);
            // on va récupérer les statuts dans un datatable puis on va parcourir les lignes(rows) de ce datatable pour 
            // construire dynamiquement les boutons radio pour le statut de l'intervenant dans son atelier

            Int16 i = 0;
            foreach (var UneLigne in listeEntitee)
            {
                //object UnControle = Activator.CreateInstance(object unobjet, unTypeControle);
                //UnControle=Convert.ChangeType(UnControle, TypeC);

                if (unTypeControle == "CheckBox")
                {
                    CheckBox UnControle = new CheckBox();
                    AffecterControle(uneWindows, UnPanel, UnControle, pPrefixe, UneLigne, i++, callback);

                }
                else if (unTypeControle == "RadioButton")
                {
                    RadioButton UnControle = new RadioButton();
                    AffecterControle(uneWindows, UnPanel, UnControle, pPrefixe, UneLigne, i++, callback);
                    UnControle.Checked += new RoutedEventHandler(callback);
                    UnControle.Unchecked += new RoutedEventHandler(callback);
                }
                i++;
            }
            UnPanel.Height = 20 * i + 5;
        }

        /// <summary>
        /// méthode permettant de remplir une combobox à partir d'une source de données
        /// </summary>
        /// <typeparam name="T">Classe de l'entité</typeparam>
        /// <param name="UneCombo"> La combobox que l'on doit remplir</param>
        /// <param name="entity">Entité qui sera la source de données de la liste</param>
        public static void RemplirComboBox<T>(ComboBox UneCombo, DbSet<T> entity) where T : class
        {
            UneCombo.ItemsSource = entity.ToList();
            UneCombo.DisplayMemberPath = "LIBELLE";
            UneCombo.SelectedValuePath = "ID";
        }

        /// <summary>
        /// méthode permettant de remplir une ListBox à partir d'une source de données
        /// </summary>
        /// <typeparam name="T">Classe de l'entité</typeparam>
        /// <param name="uneListe">Liste qui contiendra les informations</param>
        /// <param name="entity">Entité qui sera la source de données de la liste</param>
        public static void RemplirListBox<T>(ListBox uneListe, DbSet<T> entity) where T : class
        {
            uneListe.ItemsSource = entity.ToList();
            uneListe.DisplayMemberPath = "LIBELLE";
            uneListe.SelectedValuePath = "ID";
        }

        /// <summary>
        /// Cette fonction va compter le nombre de controles types CheckBox qui sont cochées contenus dans la collection controls
        /// du container passé en paramètre
        /// </summary>
        /// <param name="UnContainer"> le container sur lequel on va compter les controles de type checkbox qui sont checked</param>
        /// <returns>nombre  de checkbox cochées</returns>
        internal static int CompteChecked(Panel UnContainer)
        {
            Int16 i = 0;
            foreach (Control UnControle in UnContainer.Children)
            {
                if (UnControle.GetType().Name == "CheckBox" && (bool)((CheckBox)UnControle).IsChecked)
                {
                    i++;
                }
            }
            return i;
        }
        /// <summary>
        /// Méthode permettant les lignes pour chaque nuité
        /// </summary>
        /// <param name="uneConnexion">Objet gérant la connexion à la bdd</param>
        /// <param name="unPanel">Panel contenant les lignes</param>
        public static void creationNuitee(OracleDataBase uneConnexion, StackPanel unPanel)
        {
            int i = 0;

            foreach (VDATENUITE01 uneDate in uneConnexion.VDATENUITE01.ToList())
            {
                unPanel.Children.Add(new ResaNuite(uneDate, i, uneConnexion));
                i++;
            }
        }

        /// <summary>
        /// Méthode permettant de remettre à zéro les valeurs des champs
        /// </summary>
        /// <param name="lesGrids">Collection des grids qui seront affecter par la remise à zéro</param>
        public static void razChamps(Collection<Panel> lesGrids)
        {

            foreach (Panel uneGrid in lesGrids)
            {
                Regex rgx = new Regex("Non$");
                foreach (Object unControle in uneGrid.Children)
                {
                    if (unControle.GetType().Name == "TextBox" || unControle.GetType().Name == "MaskedTextBox")
                    {
                        ((TextBox)unControle).Text = "";
                    }
                    else if (unControle.GetType().Name == "ComboBox")
                    {
                        ((ComboBox)unControle).SelectedIndex = -1;
                    }
                    else if (unControle.GetType().Name == "RadioButton")
                    {
                        if (rgx.Match(((RadioButton)unControle).Name).Success)
                        {
                            ((RadioButton)unControle).IsChecked = true;
                        }
                        else {
                            ((RadioButton)unControle).IsChecked = false;
                        }
                    }
                    else if (unControle.GetType().Name == "CheckBox")
                    {
                        ((CheckBox)unControle).IsChecked = false;
                    }
                    else if(unControle.GetType().Name == "ListBox")
                    {
                        ((ListBox)unControle).SelectedItems.Clear();
                    }
                }

            }
        }
    }
}
