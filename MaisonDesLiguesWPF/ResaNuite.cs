﻿using MaisonDesLiguesWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MaisonDesLiguesWPF
{
    /// <summary>
    /// Classe qui correspond à une ligne d'une nuité (date nuité, hotel et catégorie de la chambre)
    /// </summary>
    public class ResaNuite : StackPanel
    {
        private CheckBox chkNuitee;
        private Label unLabel;
        private ComboBox cmbHotel;
        private ComboBox cmbType;
        private Thickness espace;
        private Int16 idNuitee;
        /// <summary>
        /// Génére une ligne avec les informations passées en paramètre
        /// </summary>
        /// <param name="dateNuitee">date de la nuitée ainsi que son id</param>
        /// <param name="i">index de la ligne</param>
        /// <param name="uneConnexion">Objet gérant la connexion Oracle</param>
        public ResaNuite(VDATENUITE01 dateNuitee, int i, OracleDataBase uneConnexion)
        {
            this.idNuitee = dateNuitee.ID;
            this.espace = new Thickness(10, 0, 0, 0);
            this.Orientation = Orientation.Horizontal;
            this.Margin = new Thickness(0, 5 * i, 0, 0);
            this.Height = 30;

            chkNuitee = new CheckBox();
            chkNuitee.Name = "chk" + dateNuitee.ID;

            unLabel = new Label();
            Thickness marginLabel = new Thickness(10, 2, 0, 0);
            unLabel.Margin = marginLabel;
            unLabel.Width = 225;
            unLabel.Content = "Nuit du " + dateNuitee.LIBELLE;

            cmbHotel = new ComboBox();
            cmbHotel.Margin = espace;
            cmbHotel.Width = 250;

            Utilitaire.RemplirComboBox<VHOTEL01>(cmbHotel, uneConnexion.VHOTEL01);
            cmbHotel.SelectedIndex = 0;

            cmbType = new ComboBox();
            cmbType.Margin = espace;
            cmbType.Width = 100;
            Utilitaire.RemplirComboBox<VCATEGORIECHAMBRE01>(cmbType, uneConnexion.VCATEGORIECHAMBRE01);
            cmbType.SelectedIndex = 0;

            this.Children.Add(chkNuitee);
            this.Children.Add(unLabel);
            this.Children.Add(cmbHotel);
            this.Children.Add(cmbType);
        }
        /// <summary>
        /// Retourne si cette ligne est coché
        /// </summary>
        /// <returns>Vrai si une nuité est coché</returns>
        public bool getNuitSelectionnee()
        {
            return (bool)this.chkNuitee.IsChecked;
        }
        /// <summary>
        /// Retourne le type de la chambre sélectionné de cette ligne
        /// </summary>
        /// <returns>Id de la catégorie de la chambre</returns>
        public String getTypeChambreSelectionnee()
        {
            return cmbType.SelectedValue.ToString();
        }
        /// <summary>
        /// Retourne l'hotêl sélectionné de cette ligne
        /// </summary>
        /// <returns>Id de l'hôtel</returns>
        public String getHotelSelectionne()
        {
            return cmbHotel.SelectedValue.ToString();
        }
        /// <summary>
        /// Retourne l'id de la date de la nuitée
        /// </summary>
        /// <returns>Id de la date de la nuitée</returns>
        public Int16 getIdNuite()
        {
            return this.idNuitee;
        }

    }
}
