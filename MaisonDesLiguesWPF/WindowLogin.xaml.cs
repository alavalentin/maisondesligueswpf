﻿using MahApps.Metro.Controls;
using MaisonDesLiguesWPF.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;

namespace MaisonDesLiguesWPF
{
    /// <summary>
    /// Logique d'interaction pour WindowLogin.xaml
    /// </summary>
    public partial class WindowLogin : MetroWindow
    {

        internal OracleDataBase oracleContext;
        internal String TitreApplication;

        public WindowLogin()
        {
            InitializeComponent();
            TitreApplication = ConfigurationManager.AppSettings["TitreApplication"];
            this.Title = TitreApplication;
        }

        private void btnConnexion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                oracleContext = new OracleDataBase();
                try {
                    
                    oracleContext.Database.Connection.ConnectionString = string.Format(ConfigurationManager.ConnectionStrings["mdlContext"].ConnectionString, TxtMdp.Password, TxtLogin.Text);
                    oracleContext.Database.Connection.Open();
                    WindowPrincipal nouvelleFenetre = new WindowPrincipal();
                    nouvelleFenetre.Owner = this;
                    nouvelleFenetre.Show();
                    this.Hide();
                }
                catch (OracleException oex)
                {
                    if (oex.Number == 1017)
                    {
                        MessageBox.Show("Login/Mot de passe invalide");
                    }
                    else
                    {
                        oracleContext.Database.Connection.ConnectionString = string.Format(ConfigurationManager.ConnectionStrings["mdlContextLycee"].ConnectionString, TxtMdp.Password, TxtLogin.Text);
                        oracleContext.Database.Connection.Open();
                        WindowPrincipal nouvelleFenetre = new WindowPrincipal();
                        nouvelleFenetre.Owner = this;
                        nouvelleFenetre.Show();
                        this.Hide();
                    }
                }

            }
            catch (OracleException oex)
            {
                if (oex.Number == 1017)
                {
                    MessageBox.Show("Login/Mot de passe invalide");
                }
                else
                {
                    MessageBox.Show(oex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ControleValide(object sender, EventArgs e)
        {
            if (TxtLogin.Text.Length == 0 || TxtMdp.Password.Length == 0)
            {
                btnConnexion.IsEnabled = false;
            }
            else
            {
                btnConnexion.IsEnabled = true;
            }
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.ControleValide(sender, e);
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
