﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaisonDesLiguesWPF.Model
{
    /// <summary>
    /// Interface d'une Vue Oracle
    /// </summary>
    public interface IVue
    {
        /// <summary>
        /// Prototype du getter qui retourne un id
        /// </summary>
        /// <returns>Id</returns>
        String getId();
        /// <summary>
        /// Prototype du getter qui retourne un libelle
        /// </summary>
        /// <returns>Libelle</returns>
        String getLibelle();
    }
}
